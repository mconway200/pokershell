﻿using PokerShell.Core.Cards;
using PokerShell.Core.Players;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokerShell.Core
{
    public class Table
    {
        private List<Player> _tablePlayers;
        private List<Card> _communityCards;
        private double _pot;
        private double _bigBlind;
        private double _smallBlind;
        private int _maxPlayerCount;
        private int _bigBlindCurrentPostition;
        private int _currentActionPosition;

        public double CurrentBet { get; set; }

        public Table(int maxPlayers)
        {
            _maxPlayerCount = maxPlayers;
        }

        public void StartNewHand()
        {
            _bigBlindCurrentPostition++;
            _pot = 0;

            while(_tablePlayers.ElementAtOrDefault(_bigBlindCurrentPostition) == null)
            {
                _bigBlindCurrentPostition++;
            }

            _currentActionPosition = _bigBlindCurrentPostition;

            CollectBlinds();
        }

        public void PreFlopAction()
        {

        }

        public void TurnAction()
        {

        }

        public void RiverAction()
        {

        }

        public void GetCurrentPlayerAction()
        {
            var player = _tablePlayers.ElementAt(_currentActionPosition);
        }

        public void AddPlayer(Player player, int position)
        {
            _tablePlayers.Insert(position, player);
        }

        public void RemovePlayer(int position)
        {
            _tablePlayers.RemoveAt(position);
        }

        private void CollectBlinds()
        {
            // big blind
            _pot += _tablePlayers.ElementAt(_currentActionPosition).GetBet(_bigBlind);
            MoveCurrentActionPosition();

            // small blind
            _pot += _tablePlayers.ElementAt(_currentActionPosition).GetBet(_smallBlind);
            MoveCurrentActionPosition();
        }

        private void MoveCurrentActionPosition()
        {
            _currentActionPosition++;

            while (_tablePlayers.ElementAtOrDefault(_currentActionPosition) == null)
            {
                _currentActionPosition++;
            }
        }
    }
}
