﻿namespace PokerShell.Core.Players
{
    public class Player
    {
        public double BankRole { get; private set; }
        public string UserName { get; private set; }

        public Player(string userName)
        {
            UpdateUserName(userName);
            ResetBankRole();
        }

        public void UpdateUserName(string userName)
        {
            UserName = userName;
        }

        public double GetBet(double amount)
        {
            BankRole -= amount;
            return amount;
        }

        public void ResetBankRole()
        {
            BankRole = 10000;
        }
    }
}
