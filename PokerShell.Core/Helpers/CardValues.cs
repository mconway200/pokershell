﻿namespace PokerShell.Core.Helpers
{
    public static class CardValues
    {
        public static char[] Suits = { 'H', 'C', 'S', 'D' };
        public static string[] Values = { "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A" };
    }
}
