﻿namespace PokerShell.Core.Cards
{
    public class Card
    {
        public char Suit { get; set; }
        public string Value { get; set; }
    }
}
