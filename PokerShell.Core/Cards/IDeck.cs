﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokerShell.Core.Cards
{
    public interface IDeck
    {
        void Shuffle();
        void RefreshDeck();
        Card GetCard();
    }
}
