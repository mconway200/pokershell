﻿using PokerShell.Core.Helpers;
using System.Collections.Generic;

namespace PokerShell.Core.Cards
{
    public class StandardDeck : IDeck
    {
        private readonly List<Card> _deck;

        public StandardDeck()
        {
            _deck = new List<Card>();
            RefreshDeck();
        }

        public Card GetCard()
        {
            return _deck.PopAt(0);
        }

        public void RefreshDeck()
        {
            foreach(var suit in CardValues.Suits)
            {
                foreach(var value in CardValues.Values)
                {
                    var card = new Card { Suit = suit, Value = value };
                    _deck.Add(card);
                }
            }
        }

        public void Shuffle()
        {
            _deck.Shuffle();
        }
    }
}
