﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace PokerShell.Console
{
    public class Program
    {
        private static readonly Socket ClientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

        public static void Main(string[] args)
        {
            System.Console.Title = "Client";
            LoopConnect();
            SendLoop();
            System.Console.ReadLine();
        }

        private static void SendLoop()
        {
            while (true)
            {
                System.Console.Write("Enter a Request: ");
                var req = System.Console.ReadLine();
                var buffer = Encoding.ASCII.GetBytes(req);
                ClientSocket.Send(buffer);

                var receivedBuffer = new byte[1024];
                var rec = ClientSocket.Receive(receivedBuffer);
                var data = new byte[rec];
                Array.Copy(receivedBuffer, data, rec);
                System.Console.WriteLine("Recieved Data: " + Encoding.ASCII.GetString(data));
            }
        }

        private static void LoopConnect()
        {
            var attempts = 0;
            while (!ClientSocket.Connected)
            {
                try
                {
                    attempts++;
                    ClientSocket.Connect(IPAddress.Loopback, 100);

                }
                catch (SocketException)
                {
                    System.Console.Clear();
                    System.Console.WriteLine($"Connection attempts: {attempts}");
                }
            }

            System.Console.Clear();
            System.Console.WriteLine("Connected");
        }
    }
}
